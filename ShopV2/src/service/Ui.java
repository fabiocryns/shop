package service;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.text.View;

import model.RepositoryElement;
import model.Game;
import model.Movie;
import model.Product;
import model.ProductType;
import model.Shop;
import model.Subscription;

public class Ui {
	private Shop shop;
	
	public Ui(Shop shop){
		this.shop = shop;
	}
	
	public void showMenu(){
		Subscription subscription = new Subscription();
		shop.registerObserver(subscription);
		MainView view = new MainView();
		JPanel panel = new ControlPanel(this);
		view.setContentPane(panel);
		view.setVisible(true);
	}
	
	public void addProduct(){
		ProductType[] choices = {ProductType.GAME, ProductType.MOVIE};
		ProductType type =  (ProductType) JOptionPane.showInputDialog(null,"Choose the product type: ", "Choose Product", JOptionPane.QUESTION_MESSAGE, null,choices, choices[0]);
		String title = checkInput("Title ?", "Title is empty!");
		String id = checkInput("Enter the id:", "Id is empty!");
		if(type.equals("M")){
			Product product = new Movie(id, title, "Movie");
			shop.addProduct(product);
		} else if(type.equals("G")){
			Product product = new Game(id, title, "Game");
			shop.addProduct(product);
		}
	}
	
	public void showProduct(){
		String id = checkIdExists();
		RepositoryElement product = shop.getProduct(id);
		JOptionPane.showMessageDialog(null, ((Product) product).getTitle());
	}
	
	public void showPrice(){
		String id = checkIdExists();
		RepositoryElement product = shop.getProduct(id);
		String days = checkInput("Number of days ?", "Days was empty!");
		JOptionPane.showMessageDialog(null, ((Product) product).getPrice(Integer.parseInt(days)));
	}

	public String checkIdExists(){
		String id = checkInput("Product id?", "Id is empty!");
		return id;
	}
	
	public String checkInput(String question, String errorMessage){
		String string = JOptionPane.showInputDialog(question);
		while(string.isEmpty() || string == null){
			JOptionPane.showMessageDialog(null, errorMessage);
			string = JOptionPane.showInputDialog(question); 
		}
		return string;
	}
	
	public void exit(){	
	}
	
	public String toString(){
		return "test Ui";
	}

}
