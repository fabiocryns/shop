package service;

import model.Shop;

public class Launcher {

	public static void main(String[] args) {
		Shop shop = new Shop();
		Ui ui = new Ui(shop);
		ui.showMenu();

	}

}
