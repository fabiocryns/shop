package service;

import java.lang.reflect.InvocationTargetException;

import javax.swing.JOptionPane;

import factories.ProductFactory;
import model.Customer;
import model.Product;
import model.ProductType;
import model.Shop;
import model.Subscription;

public class TestLauncher {

	public static void main(String[] args) {
		//Used for quick testing
		Customer customer = new Customer("0", "Fabio", "Cryns", "Crynsfabio@hotmail.com");
		
		Shop shop = new Shop();
		Subscription subscription = new Subscription();
		shop.registerObserver(subscription);
		shop.addCustomer(customer);
		shop.sendMail();
	}
}
