package service;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DownActionListener implements ActionListener {
	private Ui ui;
	
	public DownActionListener(Ui ui){
		this.ui = ui;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()){
			case "Add Product" : 
				ui.addProduct();
			break;
			case "Show Product" :
				ui.showProduct();
			break;
			case "Show Products" :
				System.out.println("not yet implemented");
			break;
			case "Show Price Product" :
				ui.showPrice();
			break;
			case "Add Customer" :
				System.out.println("not yet implemented");
			break;
			case "Subscribe" :
				System.out.println("not yet implemented");
			break;
			case "Unsubscribe" :
				System.out.println("not yet implemented");
			break;
		}
	}
}
