package service;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

public class ControlPanel extends JPanel {
	private Ui ui;
	private JButton addProductButton = new JButton("Add Product"),
			showProductButton = new JButton("Show Product"),
			showProductsButton = new JButton("Show Products"),
			priceProductButton = new JButton("Show Price Product"),
			addCustomerButton = new JButton("Add Customer"),
			subscribeNewsletterButton = new JButton("Subscribe"),
			unsubscribeButton = new JButton("Unsubscribe");
	
	public ControlPanel(Ui ui){
		this.ui = ui;
		content();
	}
	
	private void content() {
		this.setLayout(new GridLayout(7, 1));
		this.add(addProductButton);
		this.add(showProductButton);
		this.add(showProductsButton);
		this.add(priceProductButton);
		this.add(addCustomerButton);
		this.add(subscribeNewsletterButton);
		this.add(unsubscribeButton);
		enableButtons();
	}
	
	private void enableButtons(){
		addProductButton.addActionListener(new DownActionListener(ui));
		showProductButton.addActionListener(new DownActionListener(ui));
		showProductsButton.addActionListener(new DownActionListener(ui));
		priceProductButton.addActionListener(new DownActionListener(ui));
		addCustomerButton.addActionListener(new DownActionListener(ui));
		subscribeNewsletterButton.addActionListener(new DownActionListener(ui));
		unsubscribeButton.addActionListener(new DownActionListener(ui));
	}

}
