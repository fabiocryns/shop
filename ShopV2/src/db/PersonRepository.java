package db;

import java.sql.SQLException;
import java.util.List;


import model.Customer;

public interface PersonRepository {

	
	public Customer getCustomer(String userid);

	public void addCustomer(Customer customer);

	public void removeCustomer(String customerid);

	public void updateCustomer(Customer customer);

	public List<Customer> getAllCustomers();

}