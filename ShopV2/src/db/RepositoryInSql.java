package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import model.Cd;
import model.Customer;
import model.Game;
import model.Movie;
import model.Product;
import model.State;

public class RepositoryInSql implements Repository {

	private Statement statement;
	private ResultSet result;
	private volatile static RepositoryInSql uniqueInstance;

	public RepositoryInSql() {
		Properties properties = new Properties();
		String url = "jdbc:postgresql://gegevensbanken.khleuven.be:51516/2TX32";
		properties.setProperty("user", "r0622427");
		properties.setProperty("password", "D1tIsMijnT0led0ww");
		properties.setProperty("ssl", "true");
		properties.setProperty("sslfactory", "org.postgresql.ssl.NonValidatingFactory");
		Connection connection;
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(url, properties);
			statement = connection.createStatement();
		} catch (SQLException e) {
			throw new DbException(e.getMessage(), e);
		} catch (ClassNotFoundException e) {
			throw new DbException(e.getMessage(), e);
		}
	}

	public static RepositoryInSql getInstance() {
		if (uniqueInstance == null) {
			synchronized (RepositoryInSql.class) {
				if (uniqueInstance == null) {
					uniqueInstance = new RepositoryInSql();
				}
			}

		}
		return uniqueInstance;
	}

	@Override
	public Customer getCustomer(String customerid) {
		// Don't initialise with null -> nullpointerexception
		Customer customer = null;
		try {
			result = statement.executeQuery("SELECT * FROM flapio.customers WHERE customerid = '" + customerid + "'");
			while (result.next()) {
				String email = result.getString("email");
				String firstname = result.getString("firstname");
				String lastname = result.getString("lastname");
				customer = new Customer(customerid, email, firstname, lastname);
			}
			return customer;
		} catch (SQLException e) {
			throw new DbException(e.getMessage());
		}
	}

	@Override
	public void addCustomer(Customer customer) {
		if (customer == null || getAllCustomers().contains(customer)) {
			throw new DbException("Nothing to add.");
		}
		String sql = "INSERT INTO flapio.customers (customerid, firstname, lastname, email)" + "VALUES ('"
				+ customer.getId() + "', '" + customer.getFirstName() + "', '" + customer.getLastName() + "', '"
				+ customer.getEmail() + "');";
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			throw new DbException(e.getMessage());
		}
	}

	@Override
	public void removeCustomer(String customerid) {
		if (customerid == null) {
			throw new DbException("Nothing to remove.");
		}
		String sql = "DELETE  FROM flapio.customers WHERE customerid = '" + customerid + "';";
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			throw new DbException(e.getMessage());
		}
	}
	
	@Override
	public void updateCustomer(Customer customer) {
		removeCustomer(customer.getId());
		addCustomer(customer);
	}
	
	@Override
	public List<Customer> getAllCustomers() {
		List<Customer> all = new ArrayList<Customer>();
		try {
			result = statement.executeQuery("SELECT * FROM flapio.customers");
			while (result.next()) {
				String userid = result.getString("customerid");
				String email = result.getString("email");
				String password = "";
				String firstname = result.getString("firstname");
				String lastname = result.getString("lastname");
				all.add(new Customer(userid, firstname, lastname, email));
			}
			return all;
		} catch (SQLException e) {
			throw new DbException(e.getMessage());
		}
	}

	@Override
	public Product getProduct(String productId) {
		if (productId.isEmpty()) {
			throw new DbException("No id given.");
		}
		//Don't initialise with null -> nullpointerexception
		Product product = new Movie("0","testMovie","Movie");
		try {
			result = statement.executeQuery("SELECT * FROM flapio.products WHERE productid = '" + productId + "'");
			while (result.next()) {
				String productid = result.getString("productid");
				String description = result.getString("title");
				String type = result.getString("type");
				
				String state = result.getString("state");
				if (type.equals("Cd")) {
					product = new Cd(productid, description, type);
				} else if (type.equals("Game")) {
					product = new Game(productid, description, type);
				} else if (type.equals("Movie")) {
					product = new Movie(productid, description, type);
				}
				// ook de status nog setten
			}
			return product;
		} catch (SQLException e) {
			throw new DbException(e.getMessage());
		}
	}

	@Override
	public void addProduct(Product product) {
		if (product == null || getAllProducts().contains(product)) {
			throw new DbException("Nothing to add.");
		}
		String sql = "INSERT INTO flapio.products (productid, title, type, state)" + "VALUES ('" + product.getId().toString()
				+ "', '" + product.getTitle() + "', '" + product.getCategory() + "', '"
				+ product.getCurrentState().toString() + "');";
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			throw new DbException(e.getMessage());
		}
	}

	@Override
	public void updateProduct(Product product) {
		removeProduct(product.getId());
		addProduct(product);
	}

	@Override
	public void removeProduct(String productId) {
		if (productId == null) {
			throw new DbException("Nothing to remove.");
		}
		String sql = "DELETE  FROM  flapio.products WHERE productid = '" + productId + "';";
		try {
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			throw new DbException(e.getMessage());
		}
	}

	@Override
	public List<Product> getAllProducts() {
		List<Product> all = new ArrayList<Product>();

		try {
			result = statement.executeQuery("SELECT * FROM flapio.products");
			while (result.next()) {
				String productid = result.getString("productid");
				String title = result.getString("title");
				String type = result.getString("type");
				String state = result.getString("state");
				if (type.equals("cd")) {
					all.add(new Cd(productid, title, type));
				} else if (type.equals("game")) {
					all.add(new Game(productid, title, type));
				} else if (type.equals("movie")) {
					all.add(new Movie(productid, title, type));
				}
			}
			return all;
		} catch (SQLException e) {
			throw new DbException(e.getMessage());
		}
	}

}
