package db;

import java.util.List;

import model.Customer;
import model.Product;

public interface Repository {
	//PersonRepository
	public Customer getCustomer(String userid);
	public void addCustomer(Customer customer);
	public void removeCustomer(String customerid);
	public void updateCustomer(Customer customer);
	public List<Customer> getAllCustomers();
	
	//ProductRepository
	public Product getProduct(String productId) ;
	public List<Product> getAllProducts();
	public void addProduct(Product product);
	public void updateProduct(Product product);
	public void removeProduct(String productId);
}
