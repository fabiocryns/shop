package db;

import java.util.List;
import model.Product;

public interface ProductRepository {

	
	public Product getProduct(String productId) ;

	public List<Product> getAllProducts();

	public void addProduct(Product product);

	public void updateProduct(Product product);

	public void removeProduct(String productId);

}
