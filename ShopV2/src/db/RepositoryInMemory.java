package db;

import java.util.ArrayList;
import java.util.List;

import model.Customer;
import model.Product;
import model.RepositoryElement;

public class RepositoryInMemory implements Repository {
	private List<Customer> customerList = new ArrayList<Customer>(100);
	private List<Product> productList = new ArrayList<Product>(100);
	private volatile static RepositoryInMemory uniqueInstance;
	
	public static RepositoryInMemory getInstance() {
		if (uniqueInstance == null) {
			synchronized (RepositoryInSql.class) {
				if (uniqueInstance == null) {
					uniqueInstance = new RepositoryInMemory();
				}
			}
		}
		return uniqueInstance;
	}
	
	@Override
	public Customer getCustomer(String userid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addCustomer(Customer customer) {
		// TODO Auto-generated method stub
		customerList.add(customer);
	}

	@Override
	public void removeCustomer(String customerid) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateCustomer(Customer customer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Customer> getAllCustomers() {
		// TODO Auto-generated method stub
		return customerList;
	}

	@Override
	public Product getProduct(String productId) {
		// TODO Auto-generated method stub
		return productList.get(Integer.parseInt(productId));
	}

	@Override
	public List<Product> getAllProducts() {
		// TODO Auto-generated method stub
		return productList;
	}

	@Override
	public void addProduct(Product product) {
		// TODO Auto-generated method stub
		productList.add(product);
	}

	@Override
	public void updateProduct(Product product) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeProduct(String productId) {
		// TODO Auto-generated method stub
		
	}



}
