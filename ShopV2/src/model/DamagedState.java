package model;

public class DamagedState implements State {
	Product product;
	
	public DamagedState(Product product){
		this.product = product;
	}
	@Override
	public void rent() {
		throw new DomainException("Damaged -> rent not possible");
	}

	@Override
	public void bringback(boolean bool) {
		throw new DomainException("Damaged -> bringback not possible");
	}

	@Override
	public void repair() {
		product.setCurrentState(product.getAvailableState());
	}

	@Override
	public void remove() {
		product.setCurrentState(product.getRemovedState());
	}
	
	@Override
	public String getState() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String toString() {
		return "DamagedState";
	}

}
