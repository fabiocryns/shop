package model;

public class Movie extends Product {

	public Movie(String id, String title, String category) {
		super(id, title,category);
	}

	@Override
	public double getPrice(double days) {
		double price = 5;
		double daysLeft = days - 3;
		if (daysLeft > 0) {
			price += (daysLeft * 2);
		}
		return price;
	}
	

}
