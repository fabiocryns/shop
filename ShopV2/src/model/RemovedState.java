package model;

public class RemovedState implements State {
	Product product;
	public RemovedState(Product product){
		this.product = product;
	}
	@Override
	public void rent() {
		throw new DomainException("Removed -> rent not possible");
	}

	@Override
	public void bringback(boolean bool) {
		throw new DomainException("Removed -> bringback not possible");
	}

	@Override
	public void repair() {
		throw new DomainException("Removed -> repair not possible");
	}

	@Override
	public void remove() {
		throw new DomainException("Removed -> remove not possible");
	}
	
	@Override
	public String getState() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String toString() {
		return "RemovedState";
	}

}
