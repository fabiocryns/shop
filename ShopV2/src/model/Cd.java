package model;

public class Cd extends Product {

	public Cd(String id, String title, String category) {
		super(id, title, category);
	}

	@Override
	public double getPrice(double days) {
		return days * 1.5;
	}

	

}
