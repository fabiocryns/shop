package model;

public class RepositoryElement {
	public String id;
	
	public RepositoryElement(String id){
		setId(id);
	}
	
	public String getId(){
		return this.id;
	}
	public void setId(String id) {
		if (id == null || id.isEmpty()) {
			throw new DomainException("No id given.");
		}
		this.id = id;
	}

}
