package model;

public class Customer extends RepositoryElement {

	private String email , firstName, lastName;

	public Customer(String userid, String firstName, String lastName, String email) {
		super(userid);
		setFirstName(firstName);
		setLastName(lastName);
		setEmail(email);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		if (firstName == null || firstName.isEmpty()) {
			throw new DomainException("No firstName given.");
		}
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		if (lastName == null || lastName.isEmpty()) {
			throw new DomainException("No lastName given.");
		}
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		if (email == null || email.isEmpty()) {
			throw new DomainException("No email given.");
		}
		this.email = email;
	}
}
