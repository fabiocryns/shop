package model;

public abstract class Product extends RepositoryElement {
	private String title;
	private String category;
	
	private State availableState;
	private State removedState;
	private State damagedState;
	private State rentedState;
	private State currentState;
	
	
	
	public Product(String id, String title, String category){
		super(id);
		availableState = new AvailableState(this);
		removedState = new RemovedState(this);
		damagedState = new DamagedState(this);
		rentedState = new RentedState(this);
		setCurrentState(availableState);
		setTitle(title);
		setCategory(category);
	}
	
	private void setCategory(String category) {
		if (category== null){
			throw new DomainException("No category given");
		}
		this.category = category;
		
	}
	public String getCategory(){
		return this.category;
	}
	public void rent(){
		this.getCurrentState().rent();
	}
	public void bringback(boolean damaged){
		this.getCurrentState().bringback(damaged);
	}
	public void repair(){
		this.getCurrentState().repair();
	}
	public void remove(){
		this.getCurrentState().remove();
	}
	
	public abstract double getPrice(double days);
	public State getAvailableState() {
		return availableState;
	}
	public void setAvailableState(State availableState) {
		this.availableState = availableState;
	}
	public State getRemovedState() {
		return removedState;
	}
	public void setRemovedState(State removedState) {
		this.removedState = removedState;
	}
	public State getDamagedState() {
		return damagedState;
	}
	public void setDamagedState(State damagedState) {
		this.damagedState = damagedState;
	}
	public State getRentedState() {
		return rentedState;
	}
	public void setRentedState(State rentedState) {
		this.rentedState = rentedState;
	}
	public State getCurrentState() {
		return currentState;
	}
	public void setCurrentState(State currentState) {
		this.currentState = currentState;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		if(title == null || title == ""){
			throw new DomainException("Title is empty!");
		}
		this.title = title;
	}
}
