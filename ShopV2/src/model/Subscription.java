package model;

import java.util.ArrayList;
import java.util.List;

public class Subscription implements Observer {
	private List<Customer> list = new ArrayList<Customer>();
	private MailService service = new MailService();
	
	@Override
	public void update(Customer customer) {
		getList().add(customer);
	}
	
	public void sendMail(){
		for(Customer customer : getList()){
			service.sendMail(customer);
		}
	}

	private List<Customer> getList() {
		return list;
	}
	private MailService getService() {
		return service;
	}
	

	
	


}
