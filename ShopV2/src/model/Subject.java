package model;

public interface Subject {

	public void registerObserver(Subscription subscription);
	public void removeObserver(Subscription subscription);
	public void notifyObserver();
	
}
