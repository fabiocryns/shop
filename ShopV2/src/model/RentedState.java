package model;

public class RentedState implements State {
	Product product;

	public RentedState(Product product) {
		this.product = product;
	}

	@Override
	public void rent() {
		throw new DomainException("Rented -> rent not possible");
	}

	@Override
	public void bringback(boolean damaged) {
		if (damaged) {
			product.setCurrentState(product.getDamagedState());
		} else {
			product.setCurrentState(product.getAvailableState());
		}
	}

	@Override
	public void repair() {
		throw new DomainException("Rented -> repair not possible");
	}

	@Override
	public void remove() {
		throw new DomainException("Rented -> remove not possible");
	}

	@Override
	public String getState() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override 
	public String toString() {
		return "RentedState";
	}
}
