package model;

public interface State {
	public void rent();
	public void bringback(boolean damaged);
	public void repair();
	public void remove();
	public String getState();
	@Override
	public String toString();
}
