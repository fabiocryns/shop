package model;

public class Game extends Product {

	public Game(String id, String title, String category) {
		super(id, title,category);
	}

	@Override
	public double getPrice(double days) {
		return days * 3;
	}
	
}
