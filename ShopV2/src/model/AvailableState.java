package model;

public class AvailableState implements State {
	Product product;
	
	public AvailableState(Product product){
		this.product = product;
	}
	@Override
	public void rent() {
		product.setCurrentState(product.getRentedState());
	}

	@Override
	public void bringback(boolean damaged) {
		throw new DomainException("Available -> bringback not possible");
	}

	@Override
	public void repair() {
		throw new DomainException("Available -> repair not possible");
	}

	@Override
	public void remove() {
		product.setCurrentState(product.getRemovedState());

	}
	
	@Override
	public String getState() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String toString() {
		return "AvailableState";
	}

}
