package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import db.Repository;
import db.RepositoryInMemory;
import db.RepositoryInSql;

public class Shop implements Subject {

	private List<Observer> observers = new ArrayList<Observer>();
	private Repository db;

	public Shop() {
		db = RepositoryInMemory.getInstance();
	}
	
	//Products
	public void addProduct(Product product) {
		db.addProduct(product);
	}

	public void removeProduct(String id) {
		db.removeProduct(id);
	}

	public Product getProduct(String id){
		return db.getProduct(id);
	}
	
	public List<Product> getAllProducts() {
		return db.getAllProducts();
	}

	public void updateProduct(Product product) {

	}
	public void rentProduct(Product product, Customer customer){
		
	}
	public void rentProduct(List<Product> products, Customer customer){
		
	}
	
	//Customers
	public void addCustomer(Customer customer) {
		db.addCustomer(customer);
		for(Observer subscription : observers){
			((Subscription) subscription).update(customer);
		}
	}
	
	public void removeCustomer(String id) {
		db.removeCustomer(id);
	}

	@Override
	public void registerObserver(Subscription subscription) {
		observers.add(subscription);
	}

	@Override
	public void removeObserver(Subscription subscription) {
		observers.remove(observers.indexOf(subscription));
	}

	@Override
	public void notifyObserver() {

	}
	
	public void sendMail(){
		for(Observer subscription : observers){
			((Subscription) subscription).sendMail();
		}
	}

}
